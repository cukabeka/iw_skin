jQuery(function ($) {
	var f = {
		'get_link_params': function (str) {
			str = str.substr(str.indexOf("&"));
			str = str.substr(0, str.lastIndexOf("'"));
			if (str === "''")
			{
				str = undefined;
			}
			return str;
		},
		'get_icon': function (filename) {
			var ext = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase(),
				extensionmap = {
					'jpg': filename,
					'jpeg': filename,
					'bmp': filename,
					'gif': filename,
					'png': filename,
					'doc': 1,
					'docx': 1,
					'eps': 1,
					'exe': 1,
					'flv': 1,
					'gz': 1,
					'java': 1,
					'mov': 1,
					'mp3': 1,
					'ogg': 1,
					'pdf': 1,
					'ppt': 1,
					'pptx': 1,
					'rar': 1,
					'rtf': 1,
					'swf': 1,
					'tar': 1,
					'tif': 1,
					'tiff': 1,
					'txt': 1,
					'wma': 1,
					'xls': 1,
					'xlsx': 1,
					'zip': 1
				};
			if (extensionmap[ext]) {
				if (extensionmap[ext] === filename) {
					return ('/redaxo/index.php?rex_img_type=rex_mediapool_preview&rex_img_file=' + filename);
				}
				return 'media/mime-' + ext + '.gif';
			}
			return 'media/mime-unknown.gif';
		},

		'link_init': function ($el) {

			var $input_id = $el.find('input[type="hidden"]'),
				$input_name = $el.find('input[type="text"]'),
				$open = $el.find('.rex-icon-file-open'),
				$delete = $el.find('.rex-icon-file-delete'),
				id = '';

			// store widget id & params for rex functions
			$el.data('id', $input_id.attr('id').split('_').pop());
			$el.data('open_params', f.get_link_params($open.attr('onclick')));

			// store titles for list icons
			$el.data('open_title', $open.attr('title'));
			$el.data('delete_title', $delete.attr('title'));

			// new open button
			$open = $('<a href="#" class="rex-file-open">' + $open.attr('title') + '</a>');
			$open.on('click', function (e) {
				e.preventDefault();
				openLinkMap('LINK_' + $el.data('id'), $el.data('open_params'));
			});

			// replace old widgetcode
			$el.empty().append($input_id, $input_name, $open);

			// check for added/ removed link
			setInterval(function () {
				var new_id = $input_id.val();
				if (new_id !== id) {
					id = new_id;
					f.link_update($el);
				}
			}, 50);

		},
		'link_update': function ($el) {

			var $list,
				$input_id = $el.find('input[type="hidden"]'),
				name = $el.find('input[type="text"]').val();

			// make new list
			if (name !== '') {
				$list = $('<ul><li><a href="#" class="rex-link-view" title="' + $el.data('open_title') + '"></a>' + name + '<a href="#" class="rex-link-delete" title="' + $el.data('delete_title') + '"></a></li></ul>');

				// delete link
				$list.on('click', '.rex-link-delete', function (e) {
					e.preventDefault();
					deleteREXLink($el.data('id'));
				});

				// view link details
				$list.on('click', '.rex-link-view', function (e) {
					var new_params = [];
					e.preventDefault();

					// get category for view link
					$.each($el.data('open_params').split('&'), function (k,v) {
						if (v.indexOf('category_id') !== -1) {
							new_params.push('category_id=' + $input_id.val());
						}
						else if (v !== '') {
							new_params.push(v);
						}
					});
					new_params = '&' + new_params.join('&');
					openLinkMap('LINK_' + $el.data('id'), new_params);
				});

				$el.find('ul').remove().end().append($list);
			}

			// delete list
			else {
				$el.find('ul').remove();
			}

		},

		'linklist_init': function ($el) {

			var $input = $el.find('input[type="hidden"]'),
				$select = $el.find('select'),
				$open = $el.find('.rex-icon-file-open'),
				$delete = $el.find('.rex-icon-file-delete'),
				linkcount = 0;

			// store widget id & params for rex functions
			$el.data('id', $input.attr('id').split('_').pop());
			$el.data('open_params', f.get_link_params($open.attr('onclick')));

			// store titles for list icons
			$el.data('open_title', $open.attr('title'));
			$el.data('delete_title', $delete.attr('title'));

			// new open button
			$open = $('<a href="#" class="rex-file-open">' + $open.attr('title') + '</a>');
			$open.on('click', function (e) {
				e.preventDefault();
				openREXLinklist($el.data('id'), $el.data('open_params'));
			});

			// replace old widgetcode
			$el.empty().append($select, $input, $open);

			// check for added/ removed files
			setInterval(function () {
				var new_linkcount = $select.children().length;
				if (new_linkcount !== linkcount) {
					linkcount = new_linkcount;
					f.linklist_update_on_count($el);
				}
			}, 50);

		},
		'linklist_update_on_count': function ($el) {

			var $list = $('<ul></ul>'),
				$select = $el.find('select');

			// populate list
			$select.children().each(function () {
				var $li = $('<li data-id="' + $(this).val() + '"><a href="#" class="rex-link-view" title="' + $el.data('open_title') + '"></a>' + $(this).text() + '<a href="#" class="rex-link-delete" title="' + $el.data('delete_title') + '"></a></li>');
				$list.append($li);
			});

			// jquery ui sortable
			$list.sortable({
				'stop': function() {
					f.linklist_update_on_sort($el);
				}
			});

			// delete link
			$list.on('click', '.rex-link-delete', function (e) {
				var idx = $(this).closest('li').prevAll().length;
				e.preventDefault();
				$select.find('option').prop('selected', false);
				$select.find('option:eq(' + idx + ')').prop('selected', true);
				deleteREXLinklist($el.data('id'));
			});

			// view link
			$list.on('click', '.rex-link-view', function (e) {
				var idx = $(this).closest('li').prevAll().length;
				e.preventDefault();
				$select.find('option').prop('selected', false);
				$select.find('option:eq(' + idx + ')').prop('selected', true);
				openREXLinklist($el.data('id'), $el.data('view_params'));
			});

			// add list/ replace old list
			$el.find('ul').remove().end().append($list);

		},
		'linklist_update_on_sort': function ($el) {

			var $list = $el.find('ul'),
				$select = $el.find('select'),
				$input = $el.find('input[type="hidden"]'),
				tmp_input_val = [],
				tmp_options = '';

			// get new order
			$list.children().each(function () {
				var $li = $(this);
				tmp_input_val.push($li.data('id'));
				tmp_options += '<option>' + $li.text() + '</option>';
			});

			// update formfields
			$select.html(tmp_options);
			$input.val(tmp_input_val.join(','));

		},

		'media_init': function ($el) {

			var $input_name = $el.find('input[type="text"]'),
				$open = $el.find('.rex-icon-file-open'),
				$add = $el.find('.rex-icon-file-add'),
				$view = $el.find('.rex-icon-file-view'),
				$delete = $el.find('.rex-icon-file-delete'),
				$toggle_view = $('<a href="#" class="rex-file-toggle-view" title="zwischen Listen-/ Vorschauansicht wechseln"></a>'),
				name = '';

			// store widget id & params for rex functions
			$el.data('id', $input_name.attr('id').split('_').pop());
			$el.data('open_params', f.get_link_params($open.attr('onclick')));
			$el.data('add_params', f.get_link_params($add.attr('onclick')));
			$el.data('view_params', f.get_link_params($view.attr('onclick')));

			// store titles for list icons
			$el.data('view_title', $view.attr('title'));
			$el.data('delete_title', $delete.attr('title'));

			// new open & add buttons
			$open = $('<a href="#" class="rex-file-open">' + $open.attr('title') + '</a>');
			$add = $('<a href="#" class="rex-file-add">' + $add.attr('title') + '</a>');
			$open.on('click', function (e) {
				e.preventDefault();
				openREXMedia($el.data('id'), $el.data('open_params'));
			});
			$add.on('click', function (e) {
				e.preventDefault();
				addREXMedia($el.data('id'), $el.data('add_params'));
			});
			$toggle_view.on('click', function (e) {
				e.preventDefault();
				$toggle_view.toggleClass('preview');
				$el.find('ul').toggleClass('preview');
			});

			// replace old widgetcode
			$el.empty().append($input_name, $open, $add, $toggle_view);

			// check for added/ removed file
			setInterval(function () {
				var new_name = $input_name.val();
				if (new_name !== name) {
					name = new_name;
					f.media_update($el);
				}
			}, 50);

		},
		'media_update': function ($el) {

			var $list,
				filename = $el.find('input[type="text"]').val(),
				$old_list = $el.find('ul');

			// make new list
			if (filename !== '') {
				$list = $('<ul><li><a href="#" class="rex-file-view" title="' + $el.data('view_title') + '"></a>' + filename + '<a href="#" class="rex-file-delete" title="' + $el.data('delete_title') + '"></a><span><span style="background-image: url(' + f.get_icon(filename) + ')"></span></span></li></ul>');

				// delete media
				$list.on('click', '.rex-file-delete', function (e) {
					e.preventDefault();
					deleteREXMedia($el.data('id'));
				});

				// view media details
				$list.on('click', '.rex-file-view', function (e) {
					e.preventDefault();
					viewREXMedia($el.data('id'), $el.data('view_params'));
				});

				// add list/ replace old list
				if ($old_list.hasClass('preview')) {
					$list.addClass('preview');
				}
				$old_list.remove().end().append($list);
			}

			// delete list
			else {
				$el.find('ul').remove();
			}

		},

		'medialist_init': function ($el) {

			var $input_name = $el.find('input[type="hidden"]'),
				$select = $el.find('select'),
				$open = $el.find('.rex-icon-file-open'),
				$add = $el.find('.rex-icon-file-add'),
				$view = $el.find('.rex-icon-file-view'),
				$delete = $el.find('.rex-icon-file-delete'),
				$toggle_view = $('<a href="#" class="rex-file-toggle-view" title="zwischen Listen-/ Vorschauansicht wechseln"></a>'),
				filecount = 0;

			// store widget id & params for rex functions
			$el.data('id', $input_name.attr('id').split('_').pop());
			$el.data('open_params', f.get_link_params($open.attr('onclick')));
			$el.data('add_params', f.get_link_params($add.attr('onclick')));
			$el.data('view_params', f.get_link_params($view.attr('onclick')));

			// store titles for list icons
			$el.data('view_title', $view.attr('title'));
			$el.data('delete_title', $delete.attr('title'));

			// new open & add buttons
			$open = $('<a href="#" class="rex-file-open">' + $open.attr('title') + '</a>');
			$add = $('<a href="#" class="rex-file-add">' + $add.attr('title') + '</a>');

			$open.on('click', function (e) {
				e.preventDefault();
				openREXMedialist($el.data('id'), $el.data('open_params'));
			});
			$add.on('click', function (e) {
				e.preventDefault();
				addREXMedialist($el.data('id'), $el.data('add_params'));
			});
			$toggle_view.on('click', function (e) {
				e.preventDefault();
				$toggle_view.toggleClass('preview');
				$el.find('ul').toggleClass('preview');
			});

			// replace old widgetcode
			$el.empty().append($select, $input_name, $open, $add, $toggle_view);

			// check for added/ removed files
			setInterval(function () {
				var new_filecount = $select.children().length;
				if (new_filecount !== filecount) {
					filecount = new_filecount;
					f.medialist_update_on_count($el);
				}
			}, 50);

		},
		'medialist_update_on_count': function ($el) {

			var $list = $('<ul></ul>'),
				$select = $el.find('select'),
				$old_list = $el.find('ul');

			// populate list
			$select.children().each(function () {
				var filename = $(this).text(),
					$li = $('<li><a href="#" class="rex-file-view" title="' + $el.data('view_title') + '"></a>' + filename + '<a href="#" class="rex-file-delete" title="' + $el.data('delete_title') + '"></a><span><span style="background-image: url(' + f.get_icon(filename) + ')"></span></span></li>');
				$list.append($li);
			});

			// jquery ui sortable
			$list.sortable({
				'stop': function() {
					f.medialist_update_on_sort($el);
				}
			});

			// delete media
			$list.on('click', '.rex-file-delete', function (e) {
				var idx = $(this).closest('li').prevAll().length;
				e.preventDefault();
				$select.find('option').prop('selected', false);
				$select.find('option:eq(' + idx + ')').prop('selected', true);
				deleteREXMedialist($el.data('id'));
			});

			// view media details
			$list.on('click', '.rex-file-view', function (e) {
				var idx = $(this).closest('li').prevAll().length;
				e.preventDefault();
				$select.find('option').prop('selected', false);
				$select.find('option:eq(' + idx + ')').prop('selected', true);
				viewREXMedialist($el.data('id'), $el.data('view_params'));
			});

			// add list/ replace old list
			if ($old_list.hasClass('preview')) {
				$list.addClass('preview');
			}
			$old_list.remove().end().append($list);

		},
		'medialist_update_on_sort': function ($el) {

			var $list = $el.find('ul'),
				$select = $el.find('select'),
				$input = $el.find('input[type="hidden"]'),
				tmp_input_val = [],
				tmp_options = '';

			// get new order
			$list.children().each(function () {
				var $li = $(this);
				tmp_input_val.push($li.text());
				tmp_options += '<option>' + $li.text() + '</option>';
			});

			// update formfields
			$select.html(tmp_options);
			$input.val(tmp_input_val.join(','));

		}
	};
	$('.rex-widget').each(function () {
		var $widget = $(this).find('div:eq(0)'),
			classes = ['rex-widget-media', 'rex-widget-medialist', 'rex-widget-link', 'rex-widget-linklist'];
		$.each(classes, function (k, v) {
			if ($widget.hasClass(v)) {
				f[v.substr(11) + '_init']($widget);
				return false;
			}
		});
	});
});