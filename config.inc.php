<?php
if ($REX['REDAXO'] === true)
{
	rex_register_extension('OUTPUT_FILTER',
		function($params) use($REX)
		{
			$assets = new iw_assets();
			$assets->add('/files/addons/be_style/plugins/iw_skin/iw_skin_normalize.css', 'iw_skin');
			$assets->add('/files/addons/be_style/plugins/iw_skin/iw_skin_main.less', 'iw_skin');
			$assets->add('/files/addons/be_style/plugins/iw_skin/iw_skin_main.js', 'iw_skin');
			$assets->add('/files/addons/be_style/plugins/iw_skin/iw_skin_rex_widget.js', 'iw_skin');
			$params['subject'] = str_replace('<link rel="stylesheet" type="text/css" href="media/css_import.css" media="screen, projection, print" />','',$params['subject']);
			$tmp = '<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" type="text/css">';
			$tmp .= $assets->get_css('iw_skin');
			$tmp .= $assets->get_js('iw_skin');
			$params['subject'] = str_replace('</head>',$tmp.'</head>',$params['subject']);
			return $params['subject'];
		}
	);
}
?>