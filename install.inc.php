<?php

	$myself = 'iw_skin';
	$error = array();

	// check for concurrent addons
	$disable_addons = array('be_style' => 'agk_skin', 'be_style' => 'rexdev_skin');

	foreach ($disable_addons as $addon => $plugin) {
		if (OOPlugin::isInstalled($addon, $plugin) || OOPlugin::isAvailable($addon, $plugin)) {
			$error[] = "Bitte folgendes Plugin zuerst deinstallieren:" . ' ' . $plugin;
		}
	}

	$depend = "iw_assets";

	if (!$ADDONSsic['status'][$depend]) {
		$error[] = "Folgendes AddOn muss installiert und aktiviert sein:" . ' ' . $depend. ' <a href="https://bitbucket.org/IngoWinter/iw_assets" target="_blank">- Download</a>';
	}

	if (count($error) == 0) {
		$REX['ADDON']['install'][$myself] = 1;
	} else {
		$REX['ADDON']['installmsg'][$myself] = '<br />' . implode($error, '<br />');
		$REX['ADDON']['install'][$myself] = 0;
	}

?>