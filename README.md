# iw_skin - be_style Plugin für Redaxo #

iw_skin ist ein less-basiertes Backend Skin für Redaxo. Zum Kompilieren der less Dateien wird iw_assets genutzt, für die Modulwidgets (Mediabutton/ -list, Linkbutton/ -list) jQuery UI.

**Abhängigkeiten**: [iw_assets](https://bitbucket.org/IngoWinter/iw_assets) / [jQuery UI](https://github.com/joachimdoerr/jquery_ui_be_style_plugin)

**Installation**: andere Skins deinstallieren, dann installieren und aktivieren

**Screenshots**: im Repo, "iw_skin_moduleingabe.png" & "iw_skin_struktur.png"

**Das Customizer Plugin funktioniert nicht mit iw_skin**

## Hinweis für Addon-Entwickler ##

In der Datei iw_skin_addons.less wird das Theme für einzelne Addons erweitert bzw. das vom Addon mitgebrachte CSS überschrieben. Bei PRs bitte diese Datei nutzen oder eine neue Datei fürs Addon erstellen.